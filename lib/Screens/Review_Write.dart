import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter/material.dart';
import 'package:eventsdemo/utils/colors/colors.dart';
class ReviewWrite extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.grey,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(
          "Review",
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
        elevation: 0,
      ),
      body: Center(
        child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
          SizedBox(
            height: 30,
          ),
          ClipRRect(
            borderRadius: BorderRadius.circular(15.0),
            child: Image.network(
              'https://via.placeholder.com/150',
              height: 80.0,
              width: 80.0,
              fit: BoxFit.fill,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Text("Michael Scott",
              style: TextStyle(fontSize: 23, fontWeight: FontWeight.bold)),
          SizedBox(
            height: 5,
          ),
          Text("Leave your review", style: TextStyle(color: Colors.grey)),
          SizedBox(
            height: 30,
          ),
          Padding(
              padding: const EdgeInsets.symmetric(vertical: 5.0),
              child: RatingBar.builder(
                onRatingUpdate: (rating) {},
                initialRating: 1,
                itemCount: 5,
                itemSize: 30.0,
                unratedColor: Colors.grey[300],
                itemBuilder: (context, _) => Icon(
                  Icons.star,
                  color: kPrimaryColor,
                ),
              )),
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text(
                "Review",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                padding: EdgeInsets.all(15.0),
                child: SingleChildScrollView(
                  child: TextField(
                    onChanged: (val) {},
                    maxLength: 400,
                    maxLines: 8,
                    style: TextStyle(fontWeight: FontWeight.bold),
                    decoration: InputDecoration.collapsed(
                        hintText: "Write something.."),
                  ),
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.grey[100],
                ),
              ),
            ]),
          ),
          OrangeButton(txt: "Submit review"),
        ]),
      ),
    );
  }
}

class OrangeButton extends StatelessWidget {
  final String txt;
  final Function onPress;
  const OrangeButton({this.txt, this.onPress});

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      minWidth: MediaQuery.of(context).size.width * 0.9,
      padding: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
      onPressed: () {
        onPress();
      },
      child: Text(txt, style: TextStyle(fontSize: 16)),
      color: kPrimaryColor,
      textColor: Colors.white,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
    );
  }
}
