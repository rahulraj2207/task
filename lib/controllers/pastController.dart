import 'dart:convert';
import 'package:eventsdemo/models/pastData.dart';
import 'package:get/state_manager.dart';
import 'package:eventsdemo/services/pastEventApi.dart';
import 'package:get/get.dart';
var data = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjE5NjcyMzAzLCJqdGkiOiI0MjJjMzVmNzEyNTE0Mjk4Yjk2Y2Y4OTcyZTE2MTFhMCIsInVzZXJfaWQiOjR9.a5A8hglZcdH6o8fGbPLhHkSHZUkGu-rTEKcj_kH_PCQ";
class PastController extends GetxController {
  RxBool isLoading = true.obs;
  // RxList<PastData> pastData = <PastData>[].obs;
  var pastData = List<PastData>().obs;
  @override
  void onInit() {
    todaysEventsFetchApi(1);
    super.onInit();
  }
  void todaysEventsFetchApi(page) async {
    try {
      isLoading(true);
      await PastEventsApi().fetchTodaysApi(token: data, data1:page).then((response) {
        if (response.statusCode == 200) {
          var events2 = pastDataFromJson(response.body);
          if (events2 != null) {
            if (page == 1) {
              pastData.value = events2;
            } else
              pastData.addAll(events2);
          }
        } else {
          print('error occured in events details Page');
        }
      });
    }finally{
      isLoading(false);
    }
  }
}

