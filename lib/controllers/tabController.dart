import 'package:eventsdemo/events.dart';
import 'package:eventsdemo/views/UpcomingEvents.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:eventsdemo/views/pastEvents.dart';



class tabController extends GetxController{
  int tabIndex = 0;
  PageController mainPageCtr;
  @override
  void onInit() {
    super.onInit();
  }


  void onTabChaged(int index) {
    tabIndex = index;
    update();
  }
}





class PageBuilder extends StatefulWidget {

  final double height;
  final double width;
  const PageBuilder({Key key, this.height, this.width}) : super(key: key);

  @override
  _PageBuilderState createState() => _PageBuilderState();
}

class _PageBuilderState extends State<PageBuilder> {


  final controller = Get.find<tabController>();

  void initState() {
    super.initState();
    //initializing controllers
    controller.mainPageCtr = PageController();
  }




  final List<Widget> pageWidgets = [
    UpcomingEvents(),
    PastEvents(),
  ];


  @override
  Widget build(BuildContext context) {
    return PageView.builder(
      controller: controller.mainPageCtr,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: 2,
      scrollDirection: Axis.horizontal,
      itemBuilder: (context, index) {
        return pageWidgets[index];
      },
    );
  }
}
