import 'dart:convert';
import 'package:eventsdemo/models/upcomingData.dart';
import 'package:get/state_manager.dart';
import 'package:eventsdemo/services/upcomingEventsApi.dart';
import 'package:get/get.dart';
var data ="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjE5NjcyMzAzLCJqdGkiOiI0MjJjMzVmNzEyNTE0Mjk4Yjk2Y2Y4OTcyZTE2MTFhMCIsInVzZXJfaWQiOjR9.a5A8hglZcdH6o8fGbPLhHkSHZUkGu-rTEKcj_kH_PCQ";
class UpcomingController extends GetxController {
  RxBool isLoading = true.obs;
  // RxList<UpcomingData> upcomingData1 = <UpcomingData>[].obs;
  var upcomingData1 = List<UpcomingData>().obs;

  @override
  void onInit() {
    todaysEventsFetchApi(1);
    super.onInit();
  }
  void todaysEventsFetchApi(page) async {
    try {
      isLoading(true);
      await UpcomingEventsApi().fetchTodaysApi(token: data, page1:page).then((response) {
        if (response.statusCode == 200) {
          var events2 = upcomingDataFromJson(response.body);
          if (events2 != null) {
            if (page == 1) {
              upcomingData1.value = events2;

            } else
              upcomingData1.addAll(events2);
          }

        } else {
          print('error occured in events details Page');
        }
      });
    }finally{
      isLoading(false);
    }
  }
}

