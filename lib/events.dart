import 'package:eventsdemo/pageBuilder.dart';
import 'package:eventsdemo/utils/Icons/widgets.dart';
import 'package:eventsdemo/views/UpcomingEvents.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:eventsdemo/controllers/tabController.dart';
import 'package:eventsdemo/controllers/upcomingController.dart';



class events extends StatefulWidget {


  @override
  _eventsState createState() => _eventsState();
}

class _eventsState extends State<events> {


  @override
  Widget build(BuildContext context) {
    Get.put(pageBuilder());
    Get.put(tabController());
    Get.put(UpcomingController());


    final size = MediaQuery.of(context).size;


    return Scaffold(
     appBar: buildAppBar(
       title: Text('Events',style: TextStyle(
         color: Colors.black,
         fontWeight: FontWeight.w500,
       ),),
       actions: [
         IconButton(onPressed: (){}, icon: Icon(
           Icons.close,
           size: 15,
         ))
       ]
     ),
      body:  SafeArea(
            child: Column(
              children: [
                SizedBox(height: 10,),


                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(7),
                    child: SizedBox(
                      height: 35,
                      width: size.width,
                      child: ColoredBox(
                        color: Color(0xffE2E2F0),
                        child: GetBuilder<tabController>(
                          builder: (tabPageCtr) {
                            AlignmentGeometry switchers() {
                              if (tabPageCtr.tabIndex == 0) {
                                return Alignment.centerLeft;
                              } else if (tabPageCtr.tabIndex == 1) {
                                return Alignment.centerRight;
                              } else {
                                return Alignment.centerLeft;
                              }
                            }

                            return Stack(
                              children: [

                                AnimatedAlign(
                                  duration: const Duration(milliseconds: 200),
                                  alignment: switchers(),
                                  child: SizedBox(
                                    width: (size.width - 20) / 2,
                                    child: Padding(
                                      padding: const EdgeInsets.all(2.0),
                                      child: ClipRRect(
                                          borderRadius: BorderRadius.circular(7),
                                          child: ColoredBox(
                                              color: Colors.white, child: const Align())),
                                    ),
                                  ),
                                ),
                                Row(
                                  children: [
                                    buildHeadingSwitchers(
                                        text: 'Upcoming',
                                        textStyle:
                                        tabPageCtr.tabIndex == 0
                                            ? Get.textTheme.subtitle1.copyWith(fontWeight: FontWeight.w600)
                                            : Get.textTheme.subtitle1.copyWith(fontWeight: FontWeight.w500,color: Colors.black54),

                                        onPressed: () {
                                           tabPageCtr.onTabChaged(0);
                                          tabPageCtr.mainPageCtr.jumpToPage(0);

                                          }),
                                    buildHeadingSwitchers(
                                        text: 'Past',
                                        textStyle:
                                        tabPageCtr.tabIndex == 1
                                            ? Get.textTheme.subtitle1.copyWith(fontWeight: FontWeight.w600)
                                            : Get.textTheme.subtitle1.copyWith(fontWeight: FontWeight.w500,color: Colors.black54),
                                        onPressed: () {
                                           tabPageCtr.onTabChaged(1);
                                           tabPageCtr.mainPageCtr.jumpToPage(1);

                                        }),
                                  ],
                                ),
                              ],
                            );
                          },
                        ),
                      ),
                    ),
                  ),
                ),
                  SizedBox(height: 10),
                   const Expanded(child: PageBuilder(),),




              ],
            )));
  }

  Expanded buildHeadingSwitchers(
      {Function onPressed, String text, TextStyle textStyle}) {
    return Expanded(
        child: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: onPressed,
            child: Align(
                child: Text(
                  text,
                  style: textStyle,
                ))));


  }


  }

