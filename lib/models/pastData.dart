// To parse this JSON data, do
//
//     final pastData = pastDataFromJson(jsonString);

import 'dart:convert';

List<PastData> pastDataFromJson(String str) => List<PastData>.from(json.decode(str).map((x) => PastData.fromJson(x)));

String pastDataToJson(List<PastData> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class PastData {
  PastData({
    this.id,
    this.profileId,
    this.profileFirstname,
    this.profileSurname,
    this.profileUsername,
    this.profileDp,
    this.eventImage,
    this.name,
    this.description,
    this.startDate,
    this.endDate,
    this.latitude,
    this.longitude,
    this.reviewRating,
    this.category,
    this.review,
    this.totalRating,
    this.participants,
    this.participantsCount,
  });

  int id;
  int profileId;
  String profileFirstname;
  String profileSurname;
  String profileUsername;
  String profileDp;
  List<dynamic> eventImage;
  String name;
  String description;
  DateTime startDate;
  DateTime endDate;
  String latitude;
  String longitude;
  bool reviewRating;
  List<Category> category;
  List<Review> review;
  int totalRating;
  List<dynamic> participants;
  int participantsCount;

  factory PastData.fromJson(Map<String, dynamic> json) => PastData(
    id: json["id"],
    profileId: json["profile_id"],
    profileFirstname: json["profile_firstname"],
    profileSurname: json["profile_surname"],
    profileUsername: json["profile_username"],
    profileDp: json["profile_dp"],
    eventImage: List<dynamic>.from(json["event_image"].map((x) => x)),
    name: json["name"],
    description: json["description"],
    startDate: DateTime.parse(json["start_date"]),
    endDate: DateTime.parse(json["end_date"]),
    latitude: json["latitude"],
    longitude: json["longitude"],
    reviewRating: json["review_rating"],
    category: List<Category>.from(json["category"].map((x) => Category.fromJson(x))),
    review: List<Review>.from(json["review"].map((x) => Review.fromJson(x))),
    totalRating: json["total_rating"],
    participants: List<dynamic>.from(json["participants"].map((x) => x)),
    participantsCount: json["participants_count"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "profile_id": profileId,
    "profile_firstname": profileFirstname,
    "profile_surname": profileSurname,
    "profile_username": profileUsername,
    "profile_dp": profileDp,
    "event_image": List<dynamic>.from(eventImage.map((x) => x)),
    "name": name,
    "description": description,
    "start_date": startDate.toIso8601String(),
    "end_date": endDate.toIso8601String(),
    "latitude": latitude,
    "longitude": longitude,
    "review_rating": reviewRating,
    "category": List<dynamic>.from(category.map((x) => x.toJson())),
    "review": List<dynamic>.from(review.map((x) => x.toJson())),
    "total_rating": totalRating,
    "participants": List<dynamic>.from(participants.map((x) => x)),
    "participants_count": participantsCount,
  };
}

class Category {
  Category({
    this.id,
    this.name,
    this.image,
  });

  int id;
  String name;
  String image;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
    id: json["id"],
    name: json["name"],
    image: json["image"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "image": image,
  };
}

class Review {
  Review({
    this.id,
    this.reviewerId,
    this.reviewerFirstname,
    this.reviewerSurname,
    this.reviewerUsername,
    this.reviewerDp,
    this.rating,
    this.review,
  });

  int id;
  int reviewerId;
  String reviewerFirstname;
  String reviewerSurname;
  String reviewerUsername;
  String reviewerDp;
  int rating;
  String review;

  factory Review.fromJson(Map<String, dynamic> json) => Review(
    id: json["id"],
    reviewerId: json["reviewer_id"],
    reviewerFirstname: json["reviewer_firstname"],
    reviewerSurname: json["reviewer_surname"],
    reviewerUsername: json["reviewer_username"],
    reviewerDp: json["reviewer_dp"],
    rating: json["rating"],
    review: json["review"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "reviewer_id": reviewerId,
    "reviewer_firstname": reviewerFirstname,
    "reviewer_surname": reviewerSurname,
    "reviewer_username": reviewerUsername,
    "reviewer_dp": reviewerDp,
    "rating": rating,
    "review": review,
  };
}
