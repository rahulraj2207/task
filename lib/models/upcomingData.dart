// To parse this JSON data, do
//
//     final upcomingData = upcomingDataFromJson(jsonString);

import 'dart:convert';

List<UpcomingData> upcomingDataFromJson(String str) => List<UpcomingData>.from(json.decode(str).map((x) => UpcomingData.fromJson(x)));

String upcomingDataToJson(List<UpcomingData> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class UpcomingData {
  UpcomingData({
    this.id,
    this.profileId,
    this.profileFirstname,
    this.profileSurname,
    this.profileUsername,
    this.profileDp,
    this.eventImage,
    this.name,
    this.description,
    this.startDate,
    this.endDate,
    this.latitude,
    this.longitude,
    this.reviewRating,
    this.category,
    this.review,
    this.totalRating,
    this.participants,
    this.participantsCount,
  });

  int id;
  int profileId;
  String profileFirstname;
  String profileSurname;
  String profileUsername;
  String profileDp;
  List<EventImage> eventImage;
  String name;
  String description;
  DateTime startDate;
  DateTime endDate;
  String latitude;
  String longitude;
  bool reviewRating;
  List<Category> category;
  List<dynamic> review;
  int totalRating;
  List<dynamic> participants;
  int participantsCount;

  factory UpcomingData.fromJson(Map<String, dynamic> json) => UpcomingData(
    id: json["id"],
    profileId: json["profile_id"],
    profileFirstname: json["profile_firstname"],
    profileSurname: json["profile_surname"],
    profileUsername: json["profile_username"],
    profileDp: json["profile_dp"],
    eventImage: List<EventImage>.from(json["event_image"].map((x) => EventImage.fromJson(x))),
    name: json["name"],
    description: json["description"],
    startDate: DateTime.parse(json["start_date"]),
    endDate: DateTime.parse(json["end_date"]),
    latitude: json["latitude"],
    longitude: json["longitude"],
    reviewRating: json["review_rating"],
    category: List<Category>.from(json["category"].map((x) => Category.fromJson(x))),
    review: List<dynamic>.from(json["review"].map((x) => x)),
    totalRating: json["total_rating"],
    participants: List<dynamic>.from(json["participants"].map((x) => x)),
    participantsCount: json["participants_count"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "profile_id": profileId,
    "profile_firstname": profileFirstname,
    "profile_surname": profileSurname,
    "profile_username": profileUsername,
    "profile_dp": profileDp,
    "event_image": List<dynamic>.from(eventImage.map((x) => x.toJson())),
    "name": name,
    "description": description,
    "start_date": startDate.toIso8601String(),
    "end_date": endDate.toIso8601String(),
    "latitude": latitude,
    "longitude": longitude,
    "review_rating": reviewRating,
    "category": List<dynamic>.from(category.map((x) => x.toJson())),
    "review": List<dynamic>.from(review.map((x) => x)),
    "total_rating": totalRating,
    "participants": List<dynamic>.from(participants.map((x) => x)),
    "participants_count": participantsCount,
  };
}

class Category {
  Category({
    this.id,
    this.name,
    this.image,
  });

  int id;
  String name;
  String image;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
    id: json["id"],
    name: json["name"],
    image: json["image"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "image": image,
  };
}

class EventImage {
  EventImage({
    this.id,
    this.image,
  });

  int id;
  String image;

  factory EventImage.fromJson(Map<String, dynamic> json) => EventImage(
    id: json["id"],
    image: json["image"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "image": image,
  };
}
