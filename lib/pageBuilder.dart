import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:eventsdemo/views/UpcomingEvents.dart';
import 'controllers/tabController.dart';

class pageBuilder extends StatefulWidget {
  final double height;
  final double width;

  const pageBuilder({Key key, this.height, this.width}) : super(key: key);
  @override
  _pageBuilderState createState() => _pageBuilderState();
}

class _pageBuilderState extends State<pageBuilder> {
  final controller = Get.find<tabController>();
  @override
  void initState() {
    super.initState();
    //initializing controllers
    controller.mainPageCtr = PageController();
  }

  final List<Widget> pageWidgets = [
    eventTile(),
    eventTile(),
  ];

  @override
  Widget build(BuildContext context) {
    return PageView.builder(
      controller: controller.mainPageCtr,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: 3,
      scrollDirection: Axis.horizontal,
      itemBuilder: (context, index) {
        return pageWidgets[index];
      },

    );
  }
}
