import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
class UpcomingEventsApi {
  Future<http.Response> fetchTodaysApi({String token,int  page1}) async {
   final client = http.Client();
    http.Response response;
    try {
      response = await client.get(Uri.parse("https://51c046c6-73be-447a-a38c-d999464b1b85.mutualevents.co/api/v1/event/upcoming/$page1/15"),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
      );
      print(Uri.parse("https://51c046c6-73be-447a-a38c-d999464b1b85.mutualevents.co/api/v1/event/upcoming/$page1/7/"));

      print(response.body);
      print(response.statusCode);
      // var jsonString = response.body;
      return response;
    } catch (error) {
      print('this is my error');
      print(error.toString());
      throw 'throw response check';
    }
  }
}