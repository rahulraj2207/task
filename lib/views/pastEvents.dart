import 'package:eventsdemo/controllers/pastController.dart';
import 'package:eventsdemo/events.dart';
import 'package:eventsdemo/utils/Icons/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:get/get.dart';
import 'package:flutter/widgets.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:eventsdemo/models/pastData.dart';
import 'package:intl/intl.dart';
import 'package:cached_network_image/cached_network_image.dart';


class PastEvents extends StatefulWidget {

  final PastController searchController=Get.put(PastController());



  @override
  _PastEventsState createState() => _PastEventsState();
}

class _PastEventsState extends State<PastEvents> {
  final PastController controller = Get.put(PastController());

  ScrollController scrollController = ScrollController();
  var page = 1;



  void initState() {

    print('initstaterahulpast');
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      paginateTask();
    });
  }
  void paginateTask() {
    print(scrollController.position.maxScrollExtent.toString() + "max");
    print(scrollController.position.pixels);
    scrollController.addListener(() {
      if (scrollController.position.pixels ==
          scrollController.position.maxScrollExtent) {
        print("reached end");
        page++;
        controller
            .todaysEventsFetchApi(page);
      }
    });
  }


  @override
  Widget build(BuildContext context) {

    final size = MediaQuery.of(context).size;


    return Padding(
      padding: const EdgeInsets.only(left: 9,right: 9),
      child: SingleChildScrollView(
        controller: scrollController,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 9.0),
              child: buildCaptions(text: 'Your events'),
            ),
            SizedBox(height: 10),
            Container(
              child: Obx(
                    () {
                  if (controller.pastData.isEmpty)
                    return Center(child: CircularProgressIndicator());
                  else
                    return ListView.builder(
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount:controller.pastData == null ? null : controller.pastData.length,
                      itemBuilder: (context, i)
                      {
                        return eventTile(
                          eventImageUrl: controller.pastData[i].profileDp,
                          eventTitle: controller.pastData[i].name,
                          eventTimeAndKM: DateFormat.yMMMMd()
                              .format(controller.pastData[i].startDate),
                          rating: controller.pastData[i].totalRating,
                          profileName: controller.pastData[i].profileFirstname,
                          dp: controller.pastData[i].profileDp,
                          // images: controller.pastData[i].eventImage[i].image,
                        );
                      },
                    );
                },
              ),
            ),

            // SizedBox(height: 20),
            // buildCaptions(text: 'People\'s events'),
            //
            // SizedBox(height: 20),
            //
            // SizedBox(
            //   height: 350,
            //   width: size.width,
            //   child: Obx(
            //         () {
            //       if (controller.isLoading.value)
            //         return Center(child: CircularProgressIndicator());
            //       else return ListView.builder(
            //           itemCount: controller.pastData.length,
            //           itemBuilder: (context, i) {
            //             return eventTile2(
            //               eventImageUrl: controller.pastData[i].profileDp,
            //               eventTitle: controller.pastData[i].name,
            //               eventTimeAndKM: DateFormat.yMMMMd()
            //                   .format(controller.pastData[i].startDate),
            //               rating: controller.pastData[i].totalRating,
            //               profileName: controller.pastData[i].profileFirstname,
            //               dp: controller.pastData[i].profileDp,
            //               // images:
            //               // controller.pastData[i].eventImage[i].image,
            //             );
            //           },
            //         );
            //     },
            //   ),
            // ),




          ],
        ),
      ),
    );
  }
}



class eventTile extends StatelessWidget {



  final String eventImageUrl;
  final String eventTitle;
  final String eventTimeAndKM;
  final int rating;
  final String profileName;
  final String dp;

  const eventTile(
      {Key key, this.eventImageUrl, this.eventTitle, this.eventTimeAndKM, this.rating,this.profileName,this.dp});


  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      height: 100,
      margin: EdgeInsets.symmetric(horizontal: size.width * 0.025, vertical: 8),
      width: size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: SizedBox(
                height: 60,
                width: 60,
                child:eventImageUrl==null?null: CachedNetworkImage(
                  imageUrl: eventImageUrl,
                  fit: BoxFit.cover,
                )
            ),
          ),
          SizedBox(
            width: size.width * 0.03,
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  eventTitle,
                  style: Get.textTheme.subtitle1.copyWith(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                ),
                SizedBox(height: 8,),
                Text("$eventTimeAndKM * 2.4KM",
                    style: Get.textTheme.subtitle2.copyWith(
                      fontWeight: FontWeight.w600,
                      color: Colors.black45,
                    )
                ),
                Align(alignment: Alignment.center,
                  child: host(hostName: 'Hosted by you',dp: dp
                  ),
                )
              ],
            ),
          ),

          Column(
            mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.end,

              children:[

                Align(alignment: Alignment.center,
                  child: Center(
                      child: SmoothStarRating(
                        color: Colors.deepOrange.shade400,
                        borderColor: Colors.grey,
                        rating: rating.toDouble(),
                        size: 18,
                        starCount: 5,

                      )),
                ),
                SizedBox(height: 7),
                Align(alignment: Alignment.bottomLeft,
                    child: imageStack())
              ]
          )
        ],

      ),
    );
  }
}

class eventTile2 extends StatelessWidget {

  final String eventImageUrl;
  final String eventTitle;
  final String eventTimeAndKM;
  final int rating;
  final String profileName;
  final String dp;
  final String images;
  // final int rating;

  const eventTile2(
      {Key key, this.eventImageUrl, this.eventTitle, this.eventTimeAndKM, this.rating,this.profileName,this.dp,this.images});
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(

      height: 100,
      margin: EdgeInsets.symmetric(horizontal: size.width * 0.025, vertical: 8),
      width: size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: SizedBox(
                height: 60,
                width: 60,
                child:eventImageUrl==null?null: CachedNetworkImage(
                  imageUrl: eventImageUrl,
                  fit: BoxFit.cover,
                )
            ),
          ),
          SizedBox(
            width: size.width * 0.03,
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  eventTitle,
                  style: Get.textTheme.subtitle1.copyWith(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                ),
                SizedBox(height: 8,),
                Text("$eventTimeAndKM * 2.4KM",
                    style: Get.textTheme.subtitle2.copyWith(
                      fontWeight: FontWeight.w600,
                      color: Colors.black45,
                    )
                ),
                Align(alignment: Alignment.center,
                  child: host(hostName: profileName,dp: dp
                  ),
                )
              ],
            ),
          ),

          Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.end,

              children:[

                Align(alignment: Alignment.topRight,
                    child: attendButton()),


                SizedBox(height: 3),



                Align(alignment: Alignment.centerRight,
                  child: Center(
                      child: SmoothStarRating(
                        color: Colors.deepOrange.shade400,
                        borderColor: Colors.grey,
                        rating: rating.toDouble(),
                        size: 18,
                        starCount: 5,

                      )),
                ),
                Align(alignment: Alignment.bottomLeft,
                    child: imageStack())
              ]
          )
        ],

      ),

    );
  }
}

class host extends StatelessWidget {

  final String dp;
  final String hostName ;

  host({Key key, this.hostName,this.dp});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Align(
          alignment: Alignment.topLeft,
          child: Container(
            height: 28,
            width: 28,
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.white60,
              ),
              shape: BoxShape.circle,
              image:
              dp ==
                  null
                  ? null
                  :
              DecorationImage(
                  image: NetworkImage(dp),
                  fit: BoxFit.cover
              ),



            ),
          ),
        ),

        SizedBox(width: 10,),
        Align(alignment: Alignment.centerLeft,
          child: Text(
            hostName,
            style:Get.textTheme.subtitle2.copyWith(
              fontWeight: FontWeight.w600,
            ),
            maxLines: 1,
          ),
        ),


      ],


    );
  }
}


class imageStack extends StatelessWidget {

  final String images;

  imageStack({Key key, this.images});


  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomRight,
      child: FittedBox(
        child: Stack(
          children: [
            for (var i = 0;
            i <
                (randomImages.length >= 3
                    ? 4
                    : randomImages.length);
            i++)
              Container(
                height: 28,
                width: 28,
                margin:
                EdgeInsets.only(left: (15 * i).toDouble()),
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.white,
                  ),
                  shape: BoxShape.circle,
                  color: i == 3 ? Color(0xffFF5917) : Color(0xffE2E2F0),
                  image: i == 3
                      ? null
                      : DecorationImage(
                      image: NetworkImage(randomImages[i]),
                      fit: BoxFit.cover),
                ),
                alignment: Alignment.center,
                child: i == 3
                    ? buildheadings(
                    text: '+${randomImages.length - 3}',
                    height: 15,
                    style: Get.textTheme.subtitle2
                        .copyWith(color: Colors.white),
                    alignment: Alignment.center)
                    : null,
              ),
          ],
        ),
      ),
    );
  }
}

class attendButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(alignment: Alignment.topRight,
      child: SizedBox(height: 23,width: 90,
        child: TextButton(onPressed: (){},
            style: TextButton.styleFrom(
                backgroundColor: Colors.green.withOpacity(0.09),
                primary: Colors.green,

                side: BorderSide(color: Colors.green,width: 0.01,)
            ),
            child: Text(
              'ATTENDING',style: TextStyle(fontSize: 8,fontWeight: FontWeight.w900),
            )),
      ),
    );
  }
}

